<?php

class Session {


    public function __construct() {
        if(empty(session_id())){
            session_start();
        }
    }

    public function create($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public function read($name) {
        if($name){
            return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
        }
        return $_SESSION;

    }

    public function update($name, $value) {
        $_SESSION[$name] = $value;

             }


    public function delete($name) {

         unset($_SESSION[$name]);

    }





}